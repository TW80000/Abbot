const express = require('express');
const UserService = require('../services/UserService');
const bcrypt = require('bcryptjs');
const Utils = require('../Utils');

const router = express.Router();

router.post('/', async (req, res, next) => {
  try {
    if (
      !req.body.hasOwnProperty('username') ||
      !(typeof req.body.username === 'string') ||
      !(req.body.username.length > 0)
    ) {
      // No username
      return res.status(400).json({
        message: 'Username cannot be blank',
        messageType: 'error'
      });
    }
    if (
      !req.body.hasOwnProperty('password') ||
      !(typeof req.body.password === 'string') ||
      !(req.body.password.length > 0)
    ) {
      // No password
      return res.status(400).json({
        message: 'Password cannot be blank',
        messageType: 'error'
      });
    }
    const user = await UserService.getUserByName(req.body.username);

    // User doesn't exist
    if (user === undefined) {
      res.status(404).json({
        message: 'There is no user with that name yet',
        messageType: 'error'
      });
    }

    // Incorrect password
    if (!await bcrypt.compare(req.body.password, user.password)) {
      res.status(401).json({
        message: 'Password was incorrect',
        messageType: 'error'
      });
    }

    // Happy path, login successful
    const token = Utils.getRandomString(32);
    UserService.setUserToken(user, token);
    return res
      .status(200)
      .cookie('token', token)
      .cookie('username', user.username)
      .send();
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      message: 'Login failed due to server error: ' + err.message,
      messageType: 'error'
    });
  }
});

module.exports = router;
