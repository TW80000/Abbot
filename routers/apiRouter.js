const express = require('express');
const BuildOrderService = require('../services/BuildOrderService');
const datastoreLib = require('@google-cloud/datastore');
const dotenv = require('dotenv').config();
const datastore = datastoreLib({
  projectId: process.env.PROJECT_ID,
  keyFilename: 'keyfile.json'
});

const router = express.Router();

router.post('/buildorders', async (req, res) => {
  if (!req.user) {
    return res
      .status(401)
      .json({ message: 'Not logged in', messageType: 'error' });
  }

  try {
    BuildOrderService.isValid(req.body);
  } catch (err) {
    return res
      .status(400)
      .json({ message: 'Malformed request', messageType: 'error' });
  }

  const id = await BuildOrderService.create(req.body);
  return res.status(201).json({
    message: 'Successfully created build order',
    messageType: 'success',
    id: id
  });
});

router.put('/buildorders', async (req, res) => {
  if (!req.user) {
    return res
      .status(401)
      .json({ message: 'Not logged in', messageType: 'error' });
  }

  const id = req.query.id;
  const buildOrder = await BuildOrderService.getBuildOrderById(id);

  if (buildOrder === undefined) {
    return res
      .status(404)
      .json({ message: 'Build order not found', messageType: 'error' });
  }

  if (buildOrder.author !== req.user.username) {
    return res.status(403).json({
      message: "You can't modify build orders that aren't yours",
      messageType: 'error'
    });
  }

  try {
    BuildOrderService.isValid(req.body);
  } catch (err) {
    return res
      .status(400)
      .json({ message: 'Malformed request', messageType: 'error' });
  }

  await BuildOrderService.update(id, req.body);
  return res.status(200).json({
    message: 'Successfully updated build order',
    messageType: 'success',
    id: id
  });
});

router.get('/buildorders', async (req, res) => {
  const page = parseInt(req.query.page);
  if (isNaN(page)) {
    return res
      .status(400)
      .json({ message: 'Must request a numeric page', messageType: 'error' });
  }
  if (page < 1) {
    return res
      .status(400)
      .json({ message: 'Page cannot be less than 1', messageType: 'error' });
  }
  /*
   * NOTE: Using offset is apparently a lot more costly than doing something
   * more complicated.
   * See https://stackoverflow.com/a/43402539/3979187
   */
  const query = datastore
    .createQuery('BuildOrder')
    .limit(25)
    .order('lastModified', { descending: true })
    .offset((page - 1) * 25);
  return res.status(200).json((await query.run())[0]);
});

router.get('/buildorders/:id', async (req, res) => {
  const buildOrder = await BuildOrderService.getBuildOrderById(req.params.id);
  console.log('Got build order: ');
  console.log(require('util').inspect(buildOrder, false, null));
  if (buildOrder === undefined) {
    return res.status(404).json({
      message: `No build order found with id ${req.params.id}`,
      messageType: 'error'
    });
  } else {
    return res.status(200).json(buildOrder);
  }
});

router.delete(/\/buildorders\/([a-zA-Z0-9]{8})/, async (req, res) => {
  const id = req.params[0];
  console.log(`Handling request: ${req.method} ${req.path}`);
  console.log('Body: ' + require('util').inspect(req.body, false, null));
  console.log(`req.user = ${req.user}`);
  try {
    if (!req.user) {
      return res
        .status(401)
        .json({ message: 'Not logged in', messageType: 'error' });
    } else {
      const buildOrder = await BuildOrderService.getBuildOrderById(id);
      if (buildOrder === undefined) {
        return res.status(404).json({
          message: 'No build order with that id found',
          messageType: 'error'
        });
      } else {
        if (buildOrder.author === req.user.username) {
          try {
            await BuildOrderService.delete(id);
            return res.status(200).json({
              message: 'Successfully deleted build order',
              messageType: 'success'
            });
          } catch (err) {
            return res.status(500).json({
              message:
                'There was a server error when trying to delete your build order',
              messageType: 'error'
            });
          }
        } else {
          return res.status(401).json({
            message: "You cannot delete someone else's build order",
            messageType: 'error'
          });
        }
      }
    }
  } catch (err) {
    console.error(err);
    return res.status(500).json({
      message:
        'There was a server error when trying to delete your build order',
      messageType: 'error'
    });
  }
});

router.get('/user/:username/buildorders', async (req, res) => {
  const page = parseInt(req.query.page);
  if (isNaN(page)) {
    return res
      .status(400)
      .json({ message: 'Must request a numeric page', messageType: 'error' });
  }
  if (page < 1) {
    return res
      .status(400)
      .json({ message: 'Page cannot be less than 1', messageType: 'error' });
  }
  const query = datastore
    .createQuery('BuildOrder')
    .filter('author', '=', req.params.username)
    .limit(25)
    .offset((page - 1) * 25);
  return res.status(200).json((await query.run())[0]);
});

module.exports = router;
