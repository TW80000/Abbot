const express = require('express');
const UserService = require('../services/UserService');
const Utils = require('../Utils');

const router = express.Router();

router.post('/', async (req, res) => {
  if (req.body.username === '' || req.body.password === '') {
    res
      .status(400)
      .json({ message: 'Missing credentials', messageType: 'error' });
  }
  try {
    const user = await UserService.getUserByName(req.body.username);
    if (user === undefined) {
      try {
        const newUser = await UserService.createUser(req.body);
        const token = Utils.getRandomString(32);
        UserService.setUserToken(newUser, token);
        res
          .status(200)
          .cookie('token', token)
          .cookie('username', newUser.username)
          .send();
      } catch (err) {
        console.error(err);
        res.status(500).json({
          message: "Couldn't reach database",
          messageType: 'error'
        });
      }
    } else {
      res
        .status(403)
        .send({ message: 'Name already taken', messageType: 'error' });
    }
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: 'There was an error on the server',
      messageType: 'error'
    });
  }
});

module.exports = router;
