import Timezone from './Timezone';
import Link from 'next/link';

const BuildOrder = props =>
  <div className={'build-order'}>
    <h1>
      {props.data.title}
    </h1>
    <p>
      Created by{' '}
      <Link href={`/buildorders?author=${props.data.author}`}>
        <a>
          {props.data.author}
        </a>
      </Link>{' '}
      on {getPrettyDate(props.data.lastModified)}
    </p>
    <p>
      {props.data.description}
    </p>
    <div className={'timeline'}>
      {props.data.timezones.map(tz =>
        <Timezone time={tz.time} steps={tz.steps} />
      )}
    </div>
    <style jsx>
      {`
        @import url('https://fonts.googleapis.com/css?family=Spectral');

        @media (min-width: 400px) {
          .build-order {
            font-size: 1.5rem;
          }
        }

        .build-order {
          font-family: 'Spectral', serif;
          font-size: 1rem;
        }

        .timeline {
          padding: 1rem 0;
        }
      `}
    </style>
  </div>;

function getPrettyDate(dateInMillis) {
  const date = new Date(dateInMillis);
  const months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  return `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`;
}

export default BuildOrder;
