import React from 'react';
import Message, { deleteMessage } from './Message';
import Router from 'next/router';

export class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      message: null,
      messageType: '',
      username: '',
      password: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({ [name]: value });
  }

  handleSubmit(path) {
    return async () => {
      this.setState({ loading: true });
      try {
        const response = await fetch(path, {
          method: 'POST',
          body: JSON.stringify(this.state),
          credentials: 'include',
          headers: new Headers({
            'Content-Type': 'application/json'
          })
        });
        if (response.status === 200) {
          Router.push('/');
        } else {
          const responseBody = await response.json();
          this.setState({
            loading: false,
            message: responseBody.message,
            messageType: responseBody.messageType
          });
        }
      } catch (err) {
        this.setState({
          loading: false,
          message: "Couldn't contact server, please check your connection.",
          messageType: 'error'
        });
      }
    };
  }

  render() {
    return (
      <div id={'login-modal'}>
        <div className={'login-modal-inner-container'}>
          {this.state.loading
            ? <div>
                Wololoading...
                <div className={'login-loading'} />
              </div>
            : <div className={'login-main'}>
                <Message
                  message={this.state.message}
                  type={this.state.messageType}
                  onDelete={deleteMessage(this)}
                />
                <div>
                  <label>Username:</label>
                  <input
                    onChange={this.handleChange}
                    type="text"
                    name="username"
                    value={this.state.username}
                  />
                </div>
                <div>
                  <label>Password:</label>
                  <input
                    onChange={this.handleChange}
                    type="password"
                    name="password"
                    value={this.state.password}
                  />
                </div>
                <div className="buttons">
                  <button
                    className="btn"
                    onClick={this.handleSubmit('/register')}
                  >
                    Register
                  </button>
                  <button className="btn" onClick={this.handleSubmit('/login')}>
                    Log in
                  </button>
                </div>
              </div>}
        </div>
        <style jsx>{`
          .login-main > div {
            margin-top: 0.6rem;
          }

          .login-loading {
            background: rgba(0, 0, 0, 0);
            width: 50px;
            height: 50px;
            animation: rotate 1s infinite linear;
            border-top: 2px solid #333;
            border-radius: 999px;
            margin: 1rem auto 0.5rem;
          }

          @keyframes rotate {
            from {
              transform: rotate(0deg);
            }

            to {
              transform: rotate(360deg);
            }
          }
        `}</style>
      </div>
    );
  }
}

export default Login;
