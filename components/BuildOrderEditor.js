import React, { Component } from 'react';
import Message, { deleteMessage } from './Message';
import BuildOrder from './BuildOrder';
import AuthService from '../services/AuthService';
import Router from 'next/router';

class BuildOrderEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hidden: true,
      title: '',
      description: '',
      message: null,
      messageType: null,
      preview: false,
      timezones: [{ time: '', steps: [{ value: '' }] }],
      username: null,
      token: null,
      buildOrderId: null,
      mode: props.mode || 'create'
    };

    if (props.buildOrder) {
      this.state.title = props.buildOrder.title;
      this.state.description = props.buildOrder.description;
      this.state.timezones = props.buildOrder.timezones;
      this.state.buildOrderId = props.buildOrder.id;
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAddTimezone = this.handleAddTimezone.bind(this);
    this.togglePreview = this.togglePreview.bind(this);
    this.getBuildOrderForPreview = this.getBuildOrderForPreview.bind(this);
  }

  componentDidMount() {
    const creds = AuthService.getCredentials();
    this.setState(creds);
    if (
      this.props.buildOrder &&
      this.props.buildOrder.author !== creds.username
    ) {
      this.setState({
        message: "Can't modify build orders that aren't yours",
        messageType: 'error'
      });
    } else {
      this.setState({ hidden: false });
    }
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });

    this.getTimezoneEditor = this.getTimezoneEditor.bind(this);
  }

  handleSubmit(path) {
    return async () => {
      this.setState({
        message: 'Saving build order...',
        messageType: 'info'
      });
      try {
        const method = this.state.mode === 'edit' ? 'PUT' : 'POST';

        if (this.state.mode === 'edit') {
          path += `?id=${this.state.buildOrderId}`;
        }

        const response = await fetch(path, {
          method: method,
          credentials: 'include',
          body: JSON.stringify({
            title: this.state.title,
            description: this.state.description,
            timezones: this.state.timezones,
            author: this.state.username
          }),
          headers: new Headers({
            'Content-Type': 'application/json'
          })
        });
        const jsonResponse = await response.json();
        this.setState({
          message: jsonResponse.message,
          messageType: jsonResponse.messageType
        });
        if (response.status === 200 || response.status === 201) {
          Router.push(`/buildorder?id=${jsonResponse.id}`);
        }
      } catch (err) {
        this.setState({
          loading: false,
          message: "Couldn't contact server, please check your connection.",
          messageType: 'error'
        });
      }
    };
  }

  handleStepChange = (timezoneIndex, stepIndex) => evt => {
    const newTimezones = this.state.timezones.map((timezone, i) => {
      if (i !== timezoneIndex) return timezone;
      const steps = timezone.steps.slice();
      steps[stepIndex] = { ...steps[stepIndex], value: evt.target.value };
      return { ...timezone, steps: steps };
    });
    this.setState({ timezones: newTimezones });
  };

  handleDeleteStep = (timezoneIndex, stepIndex) => evt => {
    const newTimezones = this.state.timezones.map((timezone, i) => {
      if (i !== timezoneIndex) return timezone;
      const steps = timezone.steps.slice();
      steps.splice(stepIndex, 1);
      return { ...timezone, steps: steps };
    });
    this.setState({ timezones: newTimezones });
  };

  handleTimeChange = timezoneIndex => evt => {
    const newTimezones = this.state.timezones.map((timezone, i) => {
      if (i !== timezoneIndex) return timezone;
      return { ...timezone, time: evt.target.value };
    });
    this.setState({ timezones: newTimezones });
  };

  handleAddStep = timezoneIndex => evt => {
    const newTimezones = this.state.timezones.map((timezone, i) => {
      if (i !== timezoneIndex) return timezone;
      return { ...timezone, steps: timezone.steps.concat({ value: '' }) };
    });
    this.setState({ timezones: newTimezones });
  };

  handleAddTimezone() {
    this.setState({
      timezones: this.state.timezones.concat({
        time: '',
        steps: [{ value: '' }]
      })
    });
  }

  handleDeleteTimezone = timezoneIndex => evt => {
    this.setState({
      timezones: this.state.timezones.filter((timezone, i) => {
        return i !== timezoneIndex;
      })
    });
  };

  handleMoveStep = (timezoneIndex, stepIndex, direction) => evt => {
    const newTimezones = this.state.timezones.map((timezone, i) => {
      if (i !== timezoneIndex) return timezone;
      const steps = timezone.steps.slice();
      const tmp = steps[stepIndex];
      if (direction === "UP") {
        steps[stepIndex] = steps[stepIndex - 1]
        steps[stepIndex - 1] = tmp;
      } else {
        steps[stepIndex] = steps[stepIndex + 1]
        steps[stepIndex + 1] = tmp;
      }
      return { ...timezone, steps: steps };
    });
    this.setState({ timezones: newTimezones });
  }

  handleMoveSection = (timezoneIndex, direction) => evt => {
    const newTimezones = this.state.timezones.slice();
    const tmp = newTimezones[timezoneIndex];
    if (direction === "UP") {
      newTimezones[timezoneIndex] = newTimezones[timezoneIndex - 1]
      newTimezones[timezoneIndex - 1] = tmp;
    } else {
      newTimezones[timezoneIndex] = newTimezones[timezoneIndex + 1]
      newTimezones[timezoneIndex + 1] = tmp;
    }
    this.setState({ timezones: newTimezones });
  }

  getTimezoneEditor() {
    return this.state.timezones.map((timezone, i) => {
      const steps = timezone.steps.map((step, j) => {
        return (
          <li key={j}>
            <div className="vertical-align">
              <textarea
                rows="1"
                type="text"
                value={step.value}
                placeholder="Enter step"
                onChange={this.handleStepChange(i, j)}
              />
              <button
                onClick={this.handleDeleteStep(i, j)}
                className={'delete-step-btn'}
              >
                &times;
              </button>
              {j > 0 ? <button onClick={this.handleMoveStep(i, j, 'UP')} className={'move-btn'}>&uarr;</button> : null}
              {j < timezone.steps.length - 1 ? <button onClick={this.handleMoveStep(i, j, 'DOWN')} className={'move-btn'}>&darr;</button> : null}
            </div>
            <style jsx>{`
              div {
                width: 100%;
              }

              textarea {
                height: 32px;
              }

              button {
                background: none;
                border: none;
                color: #888;
                cursor: pointer;
                margin-left: 0.5rem;
                cursor: pointer;
                font-size: 1rem;
              }

              .delete-step-btn:hover {
                color: #ef5350;
              }

              .move-btn:hover {
                color: #333;
              }
            `}</style>
          </li>
        );
      });
      return (
        <div className={'timezone-editor'} key={i}>
          <div className="vertical-align">
            <input
              type="text"
              placeholder={`Section ${i + 1} title`}
              value={timezone.time}
              onChange={this.handleTimeChange(i)}
            />
            <button
              onClick={this.handleDeleteTimezone(i)}
              className={'btn delete-section-btn'}
            >
              Delete section
            </button>
            {i > 0 ? <button onClick={this.handleMoveSection(i, "UP")} className={'btn'}>Move Up</button> : null}
            {i < this.state.timezones.length - 1 ? <button onClick={this.handleMoveSection(i, "DOWN")} className={'btn'}>Move Down</button> : null}
          </div>
          <ul>
            {steps}
            <button
              className={'btn add-step-btn'}
              onClick={this.handleAddStep(i)}
            >
              Add step
            </button>
          </ul>
          <style jsx>{`
            ul {
              margin: 0rem;
            }

            .delete-section-btn {
              margin-left: 1rem;
            }

            .add-step-btn {
              margin-top: 7px;
            }

            .timezone-editor {
              margin: 1rem 0;
              padding-left: 1rem;
              border-left: 0.25rem solid #ededed;
            }
          `}</style>
        </div>
      );
    });
  }

  togglePreview() {
    this.setState({ preview: !this.state.preview });
  }

  getBuildOrderForPreview() {
    return {
      timezones: this.state.timezones,
      title: this.state.title,
      author: this.state.username,
      description: this.state.description,
      lastModified: Date.now()
    };
  }

  render() {
    if (this.state.hidden)
      return (
        <Message
          message={this.state.message}
          type={this.state.messageType}
          onDelete={deleteMessage(this)}
        />
      );
    return (
      <div className={'build-order-editor'}>
        <Message
          message={this.state.message}
          type={this.state.messageType}
          onDelete={deleteMessage(this)}
        />
        <div className={'build-order-editor-controls'}>
          <button className={'btn'} onClick={this.togglePreview}>
            {this.state.preview ? 'Edit' : 'Preview'}
          </button>
        </div>
        {this.state.preview
          ? <BuildOrder data={this.getBuildOrderForPreview()} />
          : <div>
            <div>
              <label htmlFor="title">Title</label>
              <input
                id="title"
                type="text"
                name="title"
                value={this.state.title}
                onChange={this.handleInputChange}
              />
            </div>
            <div>
              <label htmlFor="description">Description</label>
              <textarea
                id="description"
                name="description"
                value={this.state.description}
                onChange={this.handleInputChange}
              />
            </div>
            {this.getTimezoneEditor()}
            <div className={'timezone-editor'}>
              <button className={'btn'} onClick={this.handleAddTimezone}>
                Add section
                </button>
            </div>
            <div>
              <button
                onClick={this.handleSubmit('/api/buildorders')}
                className={'btn'}
              >
                {this.state.mode === 'edit'
                  ? 'Save changes'
                  : 'Create Build Order'}
              </button>
            </div>
          </div>}
        <style jsx>{`
          .build-order-editor > div > div,
          .build-order-editor > * {
            margin: 1rem 0;
          }

          .build-order-editor ul {
            margin: 0;
          }

          textarea[name='description'] {
            font-size: 1rem;
            line-height: 1.5;
            width: 100%;
            box-sizing: border-box;
          }

          .timezone-editor {
            padding-left: 1rem;
            border-left: 0.25rem solid #ededed;
          }
        `}</style>
      </div>
    );
  }
}

export default BuildOrderEditor;
