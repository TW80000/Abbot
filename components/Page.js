import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import Login from '../components/Login';
import NProgress from 'nprogress';
import Router from 'next/router';
const AuthService = require('../services/AuthService');

Router.onRouteChangeStart = NProgress.start;
Router.onRouteChangeComplete = NProgress.done;
Router.onRouteChangeError = NProgress.done;

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: null,
      token: null
    };
    this.logout = this.logout.bind(this);
  }

  static getInitialProps = async ({ children }) => children;

  componentDidMount() {
    this.setState(AuthService.getCredentials());
  }

  logout() {
    this.setState({ username: null, token: null });
    const cookies = document.cookie.split(';');

    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i];
      var eqPos = cookie.indexOf('=');
      var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
    }

    Router.push(window.location.href);
  }

  render() {
    return (
      <div className="App">
        <header>
          <Head>
            <link
              rel="stylesheet"
              href="/font-awesome-4.7.0/css/font-awesome.min.css"
            />
            <style>{`
                @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro|Spectral');

                html {
                  font-family: 'Spectral', Georgia, 'Times New Roman', Times,
                    serif;
                  font-size: 1rem;
                  background-color: #fefefe;
                  color: #222;
                  line-height: 1.5;
                }

                body {
                  margin: 0;
                }

                @media (min-width: 400px) {
                  html {
                    font-size: 1.5rem;
                  }
                }

                input,
                textarea {
                  font-size: inherit;
                  font-family: 'Source Sans Pro', Helvetica, sans-serif;
                  border: 1px solid #ddd;
                  outline: none;
                  padding: 0.25rem 0.5rem;
                  margin: 0.25rem 0;
                  background: inherit;
                  border-radius: 3px;
                }

                label {
                  display: block;
                  font-family: 'Source Sans Pro', Helvetica, sans-serif;
                }

                header {
                  display: flex;
                  justify-content: space-between;
                  align-items: center;
                }

                header > div:first-child {
                  cursor: pointer;
                }

                #logo {
                  width: 64px;
                  height: 64px;
                }

                #logo-text {
                  font-size: 1.5rem;
                  margin: 0 0 0 1rem;
                }

                ul {
                  list-style-type: circle;
                }

                .App {
                  max-width: 960px;
                  margin: 1rem auto 4rem auto;
                  min-height: calc(100vh - 5rem);
                  position: relative;
                }

                .App > * {
                  margin: 1rem;
                }

                .btn-icon {
                  padding: 0.5rem;
                  font-size: 2rem;
                }

                .delete-btn {
                  background: none;
                  border: none;
                  color: #aaa;
                  font-size: 1.1rem;
                  margin-left: 0.5rem;
                  cursor: pointer;
                  outline: none;
                }

                .delete-btn:hover {
                  color: red;
                }

                .btn {
                  border: none;
                  outline: none;
                  border-radius: 3px;
                  font-family: 'Nunito', sans-serif;
                  padding: 0.5rem 1rem;
                  cursor: pointer;
                  font-size: 0.75rem;
                  color: #333;
                  background: #eee;
                  text-transform: uppercase;
                  height: 100%;
                }
                
                .btn + .btn {
                  margin-left: 0.5rem;
                }

                .btn-large {
                  font-size: 2rem;
                }

                .vertical-align {
                  display: inline-flex;
                  align-items: center;
                }

                #nprogress {
                  pointer-events: none;
                }

                #nprogress .bar {
                  background: #444;
                  position: fixed;
                  z-index: 1031;
                  top: 0;
                  left: 0;
                  width: 100%;
                  height: 2px;
                }

                #nprogress .peg {
                  display: block;
                  position: absolute;
                  right: 0px;
                  width: 100px;
                  height: 100%;
                  box-shadow: 0 0 10px #444, 0 0 5px #444;
                  opacity: 1.0;
                  transform: rotate(3deg) translate(0px, -4px);
                }
              `}</style>
          </Head>
          <Link href="/" className={'vertical-align'}>
            <div className="vertical-align">
              <img src="/abbot.png" alt="Abbot logo" id="logo" />
              <h1 id="logo-text">Abbot</h1>
            </div>
          </Link>
          {this.state.username !== null
            ? <div className="controls">
                <button className="btn" onClick={this.logout}>
                  Log out
                </button>
                <Link href="/create">
                  <button className="btn">Create</button>
                </Link>
              </div>
            : <Link href="/login">
                <button className="btn">Log in</button>
              </Link>}
        </header>
        <main>
          {this.props.children}
        </main>
        <footer>
          <span>&copy; Troy Wolters 2017</span>
          <span> &#8211; </span>
          <Link href="/about">
            <a>About</a>
          </Link>
          <span> &#8211; </span>
          <a href="https://gitlab.com/TW80000/Abbot">Source</a>
        </footer>
        <style jsx>{`
          footer {
            position: absolute;
            text-align: center;
            width: calc(100% - 2rem);
            bottom: -4rem;
          }

          footer a,
          footer span {
            font-family: 'Source Sans Pro', Helvetica, sans-serif;
            font-size: 0.6rem;
            letter-spacing: 1px;
            text-transform: uppercase;
            text-decoration: none;
          }
        `}</style>
      </div>
    );
  }
}

export default Page;
