const Message = props =>
  <div>
    {props.message
      ? <div className={`message-area message-type-${props.type}`}>
          {props.onDelete
            ? <button
                onClick={props.onDelete}
                className={'message-area-close-button'}
              >
                &times;
              </button>
            : null}
          {props.message}
        </div>
      : null}
    <style jsx>{`
      .message-area {
        color: white;
        padding: 0.5rem 0.75rem;
        border-radius: 3px;
        font-family: 'Source Sans Pro', Helvetica, sans-serif;
        text-align: center;
        position: relative;
      }

      .message-type-success {
        background: #4caf50;
      }

      .message-type-info {
        background: #42a5f5;
      }

      .message-type-error {
        background: #ef5350;
      }

      .message-area-close-button {
        position: absolute;
        right: 0px;
        top: 4px;
        line-height: 14px;
        cursor: pointer;
        background: none;
        border: none;
        color: white;
        font-size: 1rem;
        font-family: 'Source Sans Pro', Helvetica, sans-serif;
      }
    `}</style>
  </div>;

export function deleteMessage(component) {
  return () => {
    component.setState({ message: null, messageType: '' });
  };
}

export default Message;
