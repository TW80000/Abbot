const Timezone = props =>
    <div className={'timezone'}>
        <div className={'left'}>
            <span className={'time'}>
                {props.time}
            </span>
        </div>
        <div className={'right'}>
            <ul>
                {props.steps.map((step, i) =>
                    <li key={i}>
                        {step.value}
                    </li>
                )}
            </ul>
        </div>
        <style jsx>{`
            @import url('https://fonts.googleapis.com/css?family=Spectral');
            @media (min-width: 400px) {
                .timezone {
                    display: flex;
                }

                .timezone > .left {
                    border-right: 2px solid #ddd;
                    width: 20%;
                }

                .timezone > .right {
                    width: 80%;
                }

                .timezone + .timezone > div {
                    margin-top: 1rem;
                }
            }

            .timezone {
                font-family: 'Spectral', serif;
                padding-bottom: 1rem;
            }

            .timezone > div {
                box-sizing: border-box;
            }

            .timezone > div > ul {
                margin: 0;
            }
        `}</style>
    </div>;

export default Timezone;
