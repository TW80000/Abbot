import React from 'react';
import Link from 'next/link';
import Message, { deleteMessage } from './Message';
import fetch from 'isomorphic-unfetch';
const AuthService = require('../services/AuthService');

class BuildOrderList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      buildOrders: [],
      username: null,
      token: null,
      loading: true,
      message: null,
      messageType: null,
      page: 1
    };
    this.deleteBuildOrder = this.deleteBuildOrder.bind(this);
    this.loadMore = this.loadMore.bind(this);
  }

  async componentWillReceiveProps(prv, nxt) {
    this.setState(AuthService.getCredentials());
  }

  async componentDidMount() {
    this.setState(AuthService.getCredentials());
    this.setState({ loading: true });
    try {
      const buildOrders = await (await fetch(
        `${this.props.source}?page=${this.state.page}`
      )).json();
      this.setState({ buildOrders: buildOrders });
      this.setState({ loading: false });
    } catch (err) {
      this.setState({
        loading: false,
        message: 'Failed to load build orders',
        messageType: 'Error'
      });
    }
  }

  async deleteBuildOrder(id) {
    this.setState({
      message: 'Deleting build order...',
      messageType: 'info'
    });
    try {
      const response = await fetch(`/api/buildorders/${id}`, {
        method: 'DELETE',
        credentials: 'include'
      });
      const responseBody = await response.json();
      if (response.status === 200) {
        this.setState({
          buildOrders: this.state.buildOrders.filter(
            buildOrder => buildOrder.id !== id
          )
        });
      }
      this.setState({
        message: responseBody.message,
        messageType: responseBody.messageType
      });
    } catch (err) {
      this.setState({
        message: "Couldn't contact server, please check your connection.",
        messageType: 'error'
      });
    }
  }

  async loadMore() {
    await this.setState({ loading: true, page: this.state.page + 1 });
    try {
      const buildOrders = await (await fetch(
        `${this.props.source}?page=${this.state.page}`
      )).json();
      if (buildOrders.length === 0) {
        this.setState({
          message: 'No more build orders found',
          messageType: 'info',
          page: this.state.page - 1
        });
      } else {
        this.setState({
          buildOrders: this.state.buildOrders.concat(buildOrders)
        });
      }
      this.setState({ loading: false });
    } catch (err) {
      this.setState({
        loading: false,
        message: 'Failed to load build orders',
        messageType: 'error',
        page: this.state.page - 1
      });
    }
  }

  render() {
    const rows = this.state.buildOrders.map((buildOrder, i) => {
      return (
        <tr key={buildOrder.title + i.toString()}>
          <td className={'build-order-list-title'}>
            <Link href={`/buildorder?id=${buildOrder.id}`}>
              <a>
                {buildOrder.title}
              </a>
            </Link>
          </td>
          <td>
            <Link href={`/buildorders?author=${buildOrder.author}`}>
              <a>
                {buildOrder.author}
              </a>
            </Link>
          </td>
          {this.state.username === buildOrder.author
            ? <ListActions
                loading={false}
                object={buildOrder}
                onDelete={this.deleteBuildOrder}
              />
            : null}
          <style jsx>{`
            .build-order-list-title {
              cursor: pointer;
            }

            td {
              padding-right: 1rem;
            }
          `}</style>
        </tr>
      );
    });
    return (
      <div>
        <Message
          message={this.state.message}
          type={this.state.messageType}
          onDelete={deleteMessage(this)}
        />
        {this.state.loading
          ? <div className={'loading-container'}>
              <div className={'loading loading-1'} />
              <div className={'loading loading-2'} />
              <div className={'loading loading-3'} />
              <div className={'loading loading-4'} />
              <style jsx>{`
                .loading-container {
                  margin-top: 1.5rem;
                }

                .loading {
                  animation-name: fade;
                  animation-duration: 2s;
                  animation-timing-function: linear;
                  animation-iteration-count: infinite;
                  animation-direction: alternate;
                  background: #ddd;
                  border-radius: 5px;
                  height: 10px;
                  margin: 0.5rem 0;
                  opacity: 0.3;
                  transition: 1s;
                }

                .loading-1 {
                  animation-delay: 1s;
                  width: 40%;
                }

                .loading-2 {
                  animation-delay: 2s;
                  width: 60%;
                }

                .loading-3 {
                  animation-delay: 1.5s;
                  width: 30%;
                }

                .loading-4 {
                  animation-delay: 3s;
                  width: 20%;
                }

                @keyframes fade {
                  from {
                    opacity: 0.3;
                  }

                  to {
                    opacity: 1;
                  }
                }
              `}</style>
            </div>
          : <div>
              <table>
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Author</th>
                    {this.state.username ? <th /> : null}
                  </tr>
                </thead>
                <tbody>
                  {rows}
                </tbody>
                <style jsx>{`
                  th {
                    text-align: left;
                    padding-right: 1rem;
                  }
                `}</style>
              </table>
              <div className={'build-order-list-controls'}>
                <button className={'btn'} onClick={this.loadMore}>
                  Load More
                </button>
                <style jsx>{`
                  .build-order-list-controls {
                    margin: 1rem 0;
                  }
                `}</style>
              </div>
            </div>}
      </div>
    );
  }
}

const ListActions = props =>
  <td className={'vertical-align'}>
    <button
      className={'fa-btn-wrapper'}
      onClick={() => props.onDelete(props.object.id)}
    >
      {props.loading
        ? <i className={'fa fa-spinner fa-spin'} />
        : <i className={'fa fa-trash-o'} />}
    </button>
    <Link href={`/edit?id=${props.object.id}`}>
      <button className={'fa-btn-wrapper'}>
        <i className={'fa fa-pencil'} />
      </button>
    </Link>
    <style jsx>{`
      .fa-btn-wrapper {
        cursor: pointer;
        background: none;
        border: none;
        font-size: 0.75rem;
        margin-right: 0.5rem;
        padding: 0;
      }
    `}</style>
  </td>;

export default BuildOrderList;
