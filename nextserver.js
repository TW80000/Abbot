const express = require('express');
const next = require('next');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const UserService = require('./services/UserService');
const http = require('http');
const https = require('https');
const fs = require('fs');
const dotenv = require('dotenv').config();

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app
  .prepare()
  .then(() => {
    const server = express();

    server.use(cookieParser());
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({ extended: true }));

    server.use(async (req, res, next) => {
      console.log(`\nNew request came in: ${req.method} ${req.path}`);
      console.log('Body: ' + require('util').inspect(req.body, false, null));
      console.log(
        'Cookies: ' + require('util').inspect(req.cookies, false, null)
      );
      if (req.cookies.token && typeof req.cookies.token === 'string') {
        req.user = await UserService.getUserByToken(req.cookies.token);
        console.log('Token used to set req.user = ');
        console.dir(req.user);
      } else {
        req.user = undefined;
        console.log('No token in cookies, setting req.user = undefined');
      }

      next();
    });

    server.use(express.static(__dirname + '/static'));

    server.use('/api', require('./routers/apiRouter'));
    server.use('/login', require('./routers/loginRouter'));
    server.use('/register', require('./routers/registerRouter'));

    server.get('*', (req, res) => {
      return handle(req, res);
    });

    // Use HTTPS for prod and HTTP otherwise.
    if (process.env.NODE_ENV === 'production') {
      const privateKey = fs.readFileSync(
        '/etc/letsencrypt/live/abbot.rocks/privkey.pem',
        'utf8'
      );
      const certificate = fs.readFileSync(
        '/etc/letsencrypt/live/abbot.rocks/fullchain.pem',
        'utf8'
      );
      const chain = fs.readFileSync(
        '/etc/letsencrypt/live/abbot.rocks/chain.pem',
        'utf8'
      );
      const credentials = {
        key: privateKey,
        cert: certificate,
        ca: chain
      };

      http
        .createServer((req, res) => {
          res.writeHead(301, {
            Location: 'https://' + req.headers['host'] + req.url
          });
          res.end();
        })
        .listen(80);

      https.createServer(credentials, server).listen(443);
      console.log('Abbot backend listening on port 443!');
    } else {
      server.listen(80);
      console.log('Abbot backend listening on port 80!');
    }

    // server.listen(3000, err => {
    //   if (err) throw err;
    //   console.log('> Ready on http://localhost:3000');
    // });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });
