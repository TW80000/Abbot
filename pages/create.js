import BuildOrderEditor from '../components/BuildOrderEditor';
import Page from '../components/Page';

const Create = () =>
  <Page>
    <BuildOrderEditor mode={'create'} buildOrder={null} />
  </Page>;

export default Create;
