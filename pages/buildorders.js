import React from 'react';
import Page from '../components/Page';
import BuildOrderList from '../components/BuildOrderList';
import fetch from 'isomorphic-unfetch';

const BuildOrdersPage = props =>
  <Page>
    <BuildOrderList source={props.source} />
  </Page>;

BuildOrdersPage.getInitialProps = async ({ query, req }) => {
  const author = query.author;
  const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : '';
  return { source: `${baseUrl}/api/user/${author}/buildorders` };
};

export default BuildOrdersPage;
