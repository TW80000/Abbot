import React from 'react';
import Page from '../components/Page';
import BuildOrder from '../components/BuildOrder';
import fetch from 'isomorphic-unfetch';

const BuildOrderPage = props =>
  <Page>
    <BuildOrder data={props.buildOrder} />
  </Page>;

BuildOrderPage.getInitialProps = async ({ query, req }) => {
  const id = query.id;
  console.log(`id = ${id}`);
  const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : '';
  const buildOrder = await (await fetch(
    `${baseUrl}/api/buildorders/${id}`
  )).json();
  return { buildOrder: buildOrder };
};

export default BuildOrderPage;
