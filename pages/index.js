import BuildOrderList from '../components/BuildOrderList';
import Page from '../components/Page';

const Index = props =>
  <Page>
    <BuildOrderList source={'/api/buildorders'} />
  </Page>;

export default Index;
