import React, { Component } from 'react';
import Page from '../components/Page';
import BuildOrderEditor from '../components/BuildOrderEditor';
import fetch from 'isomorphic-unfetch';
import Message, { deleteMessage } from '../components/Message';

class EditPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: null,
      token: null,
      message: this.props.message || null,
      messageType: this.props.messageType || null,
      buildOrder: this.props.buildOrder
    };
  }

  render() {
    return (
      <Page>
        {this.state.message
          ? <Message
              message={this.props.message}
              type={this.props.messageType}
            />
          : <BuildOrderEditor
              mode={'edit'}
              buildOrder={this.props.buildOrder}
            />}
      </Page>
    );
  }
}

EditPage.getInitialProps = async ({ query, req }) => {
  const id = query.id;
  const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : '';
  const responseObj = await (await fetch(
    `${baseUrl}/api/buildorders/${id}`
  )).json();

  if (responseObj.hasOwnProperty('message')) {
    return responseObj;
  }

  return { buildOrder: responseObj || null };
};

export default EditPage;
