import Page from '../components/Page';

const About = () =>
  <Page>
    <h1>About Abbot</h1>
    <p>
      Abbot is a build order tool for Age of Empires II. A build order is a
      guide for what to do at the beginning of a game. For example, a build
      order might outline the order in which you should queue units and
      construct buildings. Build orders also have timestamps that show you the
      pace at which you should be moving through the build order.
    </p>
    <p>Abbot makes it easy to create and share build orders.</p>
    <p>
      Abbot is completely open source, meaning anyone can contribute features or
      bugfixes. The project is hosted on GitLab{' '}
      <a href="https://gitlab.com/TW80000/Abbot">here</a>.
    </p>
    <p>
      Abbot is in no way affiliated with the companies that develop, produce,
      and/or own Age of Empires II. Is it made by the community, for the
      community.
    </p>
  </Page>;

export default About;
