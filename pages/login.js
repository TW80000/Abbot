import Page from '../components/Page';
import Login from '../components/Login';

export default () =>
  <Page>
    <Login />
  </Page>;
