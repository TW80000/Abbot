import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import BuildOrder from './BuildOrder';
import BuildOrderEditor from './BuildOrderEditor';
import BuildOrderList from './BuildOrderList';
import Login from './Login';
import Message, { deleteMessage } from './Message';
import './App.css';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editorActive: false,
            activeBuildOrder: null,
            buildOrders: [],
            loading: false,
            loggedIn: false,
            loginModalActive: false,
            user: null,
            aboutActive: false,
            message: null,
            messageType: ''
        };
        this.edit = this.edit.bind(this);
        this.closeEditor = this.closeEditor.bind(this);
        this.getMainContent = this.getMainContent.bind(this);
        this.getBuildOrderById = this.getBuildOrderById.bind(this);
        this.setActiveBuildOrder = this.setActiveBuildOrder.bind(this);
        this.showLoginModal = this.showLoginModal.bind(this);
        this.handleUser = this.handleUser.bind(this);
        this.cancelLogin = this.cancelLogin.bind(this);
        this.closeActiveBuildOrder = this.closeActiveBuildOrder.bind(this);
        this.getBigCloseButton = this.getBigCloseButton.bind(this);
        this.showAbout = this.showAbout.bind(this);
        this.hideAbout = this.hideAbout.bind(this);
        this.deleteBuildOrder = this.deleteBuildOrder.bind(this);
    }

    edit() {
        this.setState({ editorActive: true });
    }

    closeEditor() {
        this.setState({ editorActive: false });
    }

    getBuildOrderById(id) {
        const result = this.state.buildOrders.filter(
            buildOrder => buildOrder.id === id
        );
        if (result.length === 0) {
            throw new Error(`No build order with id ${id} found!`);
        }
        return result[0];
    }

    setActiveBuildOrder(id) {
        const buildOrder = this.getBuildOrderById(id);
        this.setState({ activeBuildOrder: buildOrder });
    }

    async deleteBuildOrder(id) {
        this.setState({
            message: 'Deleting build order...',
            messageType: 'info'
        });
        try {
            const response = await fetch(`/api/buildorders/${id}`, {
                method: 'DELETE',
                body: JSON.stringify({ token: this.state.user.token }),
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            });
            const responseBody = await response.json();
            console.dir(responseBody);
            if (response.status === 200) {
                this.setState({
                    buildOrders: this.state.buildOrders.filter(
                        bo => bo.id !== id
                    )
                });
            }
            this.setState({
                message: responseBody.message,
                messageType: responseBody.messageType
            });
        } catch (err) {
            console.error(err);
            console.dir(err);
            this.setState({
                message:
                    "Couldn't contact server, please check your connection.",
                messageType: 'error'
            });
        }
    }

    getMainContent() {
        if (this.state.editorActive) {
            return <BuildOrderEditor user={this.state.user} />;
        } else if (this.state.activeBuildOrder) {
            return <BuildOrder data={this.state.activeBuildOrder} />;
        } else if (this.state.loading) {
            return <p>Loading...</p>;
        } else if (this.state.aboutActive) {
            return (
                <p>
                    Made by Troy Wolters.{' '}
                    <a href="https://gitlab.com/TW80000/Abbot">
                        Source Code on GitLab
                    </a>{' '}
                    under the MIT license.
                </p>
            );
        } else if (this.state.buildOrders.length) {
            const author = this.state.user ? this.state.user.username : null;
            return (
                <BuildOrderList
                    data={this.state.buildOrders}
                    handleClickTitle={this.setActiveBuildOrder}
                    onDelete={this.deleteBuildOrder}
                    author={author}
                />
            );
        } else {
            return (
                <p>
                    No build orders found. There may be a problem with your
                    internet connection.
                </p>
            );
        }
    }

    async loadBuildOrders() {
        try {
            this.setState({ loading: true });
            const response = await fetch('/api/buildorders');
            const json = await response.json();
            this.setState({ buildOrders: json });
        } finally {
            this.setState({ loading: false });
        }
    }

    async componentDidMount() {
        await this.loadBuildOrders();
    }

    showLoginModal() {
        this.setState({ loginModalActive: true });
    }

    handleUser(user) {
        this.setState({ user: user, loggedIn: true, loginModalActive: false });
    }

    cancelLogin() {
        this.setState({ loggedIn: false, loginModalActive: false });
    }

    closeActiveBuildOrder() {
        this.setState({ activeBuildOrder: null });
    }

    getBigCloseButton(callback) {
        return (
            <button className={'delete-btn btn-large'} onClick={callback}>
                &times;
            </button>
        );
    }

    showAbout() {
        this.setState({ aboutActive: true });
    }

    hideAbout() {
        this.setState({ aboutActive: false });
    }

    getDynamicHeaderContent() {
        if (this.state.editorActive) {
            return this.getBigCloseButton(this.closeEditor);
        } else if (this.state.activeBuildOrder) {
            return this.getBigCloseButton(this.closeActiveBuildOrder);
        } else if (this.state.aboutActive) {
            return this.getBigCloseButton(this.hideAbout);
        } else if (this.state.loggedIn) {
            return (
                <button className={'btn'} onClick={this.edit}>
                    Create
                </button>
            );
        } else {
            return (
                <button onClick={this.showLoginModal} className={'btn'}>
                    Log In
                </button>
            );
        }
    }

    render() {
        return (
            <Router>
                <div className="App">
                    <Link to="/login">Login</Link>
                    <hr />
                    <Route exact path="/" render={() => <p>Hi</p>} />
                    <Route exact path="/login" render={MyLogin.bind(this)} />
                </div>
            </Router>
            {this.state.loginModalActive
                ? <Login
                      onCancel={this.cancelLogin}
                      onUser={this.handleUser}
                  />
                : null}
            <header>
                <div onClick={this.showAbout} className={'vertical-align'}>
                    <img src="abbot.png" alt="Abbot logo" id="logo" />
                    <h1 id="logo-text">Abbot</h1>
                </div>
                {this.getDynamicHeaderContent()}
            </header>
            <Message
                message={this.state.message}
                type={this.state.messageType}
                onDelete={deleteMessage(this)}
            />
            {this.getMainContent()}
        );
    }
}

const MyLogin = () =>
    <Login onCancel={this.cancelLogin} onUser={this.handleUser} />;

const App = () =>
    <Router>
        <div>
            <Route exact path="/" component={Main} />
        </div>
    </Router>;

export default App;
