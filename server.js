const datastoreLib = require('@google-cloud/datastore');
const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const https = require('https');
const fs = require('fs');
const dotenv = require('dotenv').config();
const UserService = require('./services/UserService');
const dotenv = require('dotenv').config();
const datastore = datastoreLib({
  projectId: process.env.PROJECT_ID,
  keyFilename: 'keyfile.json'
});

const app = express();

app.set('trust proxy', 1);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Authentication
app.use(async (req, res, next) => {
  console.log(`New request came in: ${req.method} ${req.path}`);
  console.log('Body: ' + require('util').inspect(req.body, false, null));
  if (req.body.hasOwnProperty('token') && typeof req.body.token === 'string') {
    req.user = await UserService.getUserByToken(req.body.token);
    console.log('Token used to set req.user = ');
    console.dir(req.user);
  } else {
    req.user = undefined;
    console.log('No token in body, setting req.user = undefined');
  }
  next();
});

app.use(express.static(__dirname + '/build'));

app.use('/login', require('./routers/loginRouter'));
app.use('/register', require('./routers/registerRouter'));
app.use('/api', require('./routers/apiRouter'));

// Use HTTPS for prod and HTTP otherwise.
if (process.env.NODE_ENV === 'production') {
  const privateKey = fs.readFileSync(
    '/etc/letsencrypt/live/abbot.rocks/privkey.pem',
    'utf8'
  );
  const certificate = fs.readFileSync(
    '/etc/letsencrypt/live/abbot.rocks/fullchain.pem',
    'utf8'
  );
  const chain = fs.readFileSync(
    '/etc/letsencrypt/live/abbot.rocks/chain.pem',
    'utf8'
  );
  const credentials = {
    key: privateKey,
    cert: certificate,
    ca: chain
  };

  http
    .createServer((req, res) => {
      res.writeHead(301, {
        Location: 'https://' + req.headers['host'] + req.url
      });
      res.end();
    })
    .listen(80);

  https.createServer(credentials, app).listen(443);
  console.log('Abbot backend listening on port 443!');
} else {
  app.listen(80);
  console.log('Abbot backend listening on port 80!');
}
