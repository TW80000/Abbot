const datastoreLib = require('@google-cloud/datastore');
const Utils = require('../Utils');
const dotenv = require('dotenv').config();
const datastore = datastoreLib({
  projectId: process.env.PROJECT_ID,
  keyFilename: 'keyfile.json'
});

class BuildOrderService {
  isValid(buildOrder) {
    const titleValid =
      buildOrder.hasOwnProperty('title') &&
      typeof buildOrder.title === 'string' &&
      buildOrder.title.length > 0;
    if (!titleValid)
      throw new TypeError(
        'Invalid Build Order: Must have property `title` of type `string`'
      );
    const authorValid =
      buildOrder.hasOwnProperty('author') &&
      typeof buildOrder.author === 'string' &&
      buildOrder.author.length > 0;
    if (!authorValid)
      throw new TypeError(
        'Invalid Build Order: Must have property `author` of type `string`'
      );
    const descriptionValid =
      buildOrder.hasOwnProperty('description') &&
      typeof buildOrder.description === 'string';
    if (!descriptionValid)
      throw new TypeError(
        'Invalid Build Order: Must have property `description` of type `string`'
      );
    const timezonesValid =
      buildOrder.hasOwnProperty('timezones') &&
      Array.isArray(buildOrder.timezones) &&
      buildOrder.timezones.reduce((tzSum, tz) => {
        return (
          tzSum &&
          tz.hasOwnProperty('time') &&
          typeof tz.time === 'string' &&
          tz.time.length > 0 &&
          tz.hasOwnProperty('steps') &&
          tz.steps.reduce((stepSum, step) => {
            return (
              stepSum &&
              typeof step === 'object' &&
              step.hasOwnProperty('value') &&
              typeof step.value === 'string' &&
              step.value.length > 0
            );
          }, true)
        );
      }, true);
    if (!timezonesValid)
      throw new TypeError(
        'Invalid Build Order: Property `timezones` was ill-formed'
      );
  }

  async create(buildOrder) {
    const id = Utils.getRandomString(8);
    const lastModified = Date.now();
    const key = datastore.key(['BuildOrder', id]);
    await datastore.save({
      key: key,
      data: {
        title: buildOrder.title,
        author: buildOrder.author,
        description: buildOrder.description,
        timezones: buildOrder.timezones,
        id: id,
        lastModified: lastModified
      }
    });
    return id;
  }

  async update(id, buildOrder) {
    const lastModified = Date.now();
    const key = datastore.key(['BuildOrder', id]);
    await datastore.save({
      key: key,
      data: {
        title: buildOrder.title,
        author: buildOrder.author,
        description: buildOrder.description,
        timezones: buildOrder.timezones,
        id: id,
        lastModified: lastModified
      }
    });
    return id;
  }

  async delete(id) {
    const key = datastore.key(['BuildOrder', id]);
    return (await datastore.delete(key))[0];
  }

  async getBuildOrderById(id) {
    return (await datastore.get(datastore.key(['BuildOrder', id])))[0];
  }
}

module.exports = new BuildOrderService();
