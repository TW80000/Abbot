class AuthService {
  getCredentials() {
    const cookieData = document.cookie
      .split('; ')
      .map(c => c.split('='))
      .reduce((arr, val) => {
        arr[val[0]] = val[1];
        return arr;
      }, {});
    if (
      typeof cookieData.username === 'string' &&
      cookieData.username.length > 0 &&
      typeof cookieData.token === 'string' &&
      cookieData.token.length > 0
    ) {
      return { username: cookieData.username, token: cookieData.token };
    } else {
      return { username: null, token: null };
    }
  }
}

module.exports = new AuthService();
