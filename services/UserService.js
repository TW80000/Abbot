var bcrypt = require('bcryptjs');
const datastoreLib = require('@google-cloud/datastore');
const dotenv = require('dotenv').config();
const datastore = datastoreLib({
  projectId: process.env.PROJECT_ID,
  keyFilename: 'keyfile.json'
});

class UserService {
  async getUserByName(name) {
    return (await datastore.get(datastore.key(['User', name])))[0];
  }

  async setUserToken(user, token) {
    const key = datastore.key(['User', user.username]);
    const data = Object.assign({}, user, { token: token });
    return await datastore.save({ key: key, data: data });
  }

  async getUserByToken(token) {
    const query = datastore.createQuery('User').filter('token', '=', token);
    return (await datastore.runQuery(query))[0][0];
  }

  async createUser(user) {
    const key = datastore.key(['User', user.username]);
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = bcrypt.hashSync(user.password, salt);
    const data = {
      username: user.username,
      password: hashedPassword
    };
    await datastore.save({ key: key, data: data });
    return data;
  }
}

module.exports = new UserService();
