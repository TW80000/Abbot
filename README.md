![Logo of the project](./public/abbot.png)

# Abbot
> The Aoe2 Build Order Tool

ab·bot  
/ˈabət/  
*noun*  
A man who is the head of an abbey of monks.

This is a tool to create and share build orders for the game Age of Empires II.

## Developing

### Built With
Next.js with a custom express server. Production site is hosted on Google Cloud Platform and Google Cloud Datastore is used as the database.

### Prerequisites
To set up a dev environment you'll need at least node 8 and npm 5. Code should be formatted using [Prettier](https://github.com/prettier/prettier).

A Google Cloud API key is also needed to access the database and production server. When I add CI and CD I think I can add the API key and other env variables to GitLab. For now, contact me if you want to help develop Abbot and I'll set up a separate testing project with a project ID that can be made public.

### Setting up Dev

Here's a brief intro about what a developer must do in order to start developing
the project further:

```shell
git clone https://gitlab.com/TW80000/Abbot.git
cd Abbot/
npm install
```

That will download the project and install the dependencies. To start developing just run `node nextserver` in a terminal and go to town.

If you got an API key, save it as `keyfile.json` in the project folder root. You won't be able to access data without one.

You should also use environment variables when doing dev work. See the Configuration section below for how to set that up.

### Building

To build it, just run

```shell
npm run build
```

This builds an optimized production build though. You shouldn't need to do this for development.

### Deploying / Publishing
At the moment it's just me handling prod, but to do so I do the following on the server:

```shell
git pull
npm run build
sudo node nextserver
```

That pulls the latest stuff from master, builds it and runs it. I know running node as root is a no-no so I should set up a reverse proxy at some point.

I also want to set up Continuous Deployment sometime in the future.

## Versioning
This project does not use versioning right now. I am using the issue tracker in Gitlab though and working on one milestone at a time.

## Configuration
Create a file named `.env` in the root of the directory and set the appropriate environment variables in it.

Example `.env` file:

```shell
NODE_ENV="development"
```

## Tests
There are no tests yet, but there is an issue to add them.

## Style guide
Unfortunately I don't have any way of automatically checking set up on Gitlab yet, but I use Prettier in VS Code that runs every time I save a file.

## Api Reference

`GET /api/buildorders?page={1}`

* Get a list of all build orders (paginated)

`GET /api/buildorders/{id}`

* Get a build order by id

`GET /api/user/{username}/buildorders?page={1}`

* Get build orders for a given user (paginated)

`POST /api/buildorders`

* Create a build order

`PUT /api/buildorders?id={abcd1234}`

* Update a build order

`DELETE /api/buildorders/{id}`

* Delete a build order

## Database

The 'database' is Google Cloud Datastore.

## Licensing

MIT (c) Troy Wolters